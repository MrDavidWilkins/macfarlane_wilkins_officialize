(function() {
    "use strict";

})();

var playPause = document.querySelector('.playPause');
var muteButton = document.querySelector('.muteButton')
var video = document.querySelector('.videoMedia');
var teaserButton = document.querySelector('.teaserButton');
var trailerButton = document.querySelector('.trailerButton');

// functions


function videoPlay(evt) {

  if (video.paused || video.ended) {
    evt.currentTarget.innerHTML="Pause";
    video.play();
  }
  else {
    evt.currentTarget.innerHTML="Play";
    video.pause();

  }

}
function swapVideo(evt){
  video.currentTime = 0;
  if(evt.currentTarget.id==="teaser"){
    video.src = "assets/Officialize_Video.mp4";
    video.load();
  } else {
    video.src = "assets/Header_Final.mp4";
    video.load();
  }
  video.play();
}

function mute(evt) {
  if (video.muted) {
    evt.currentTarget.innerHTML="Mute";
    video.muted = false;
  }
  else {
    evt.currentTarget.innerHTML="Unmute";
    video.muted = true;
  }
}
// listeners
playPause.addEventListener( 'click',videoPlay, false);
muteButton.addEventListener('click', mute, false);
teaserButton.addEventListener('click', swapVideo, false);
trailerButton.addEventListener('click', swapVideo, false);


//this was an attempt at back to top button.
function() {
  var backButton = document.querySelector('.backToTop');

  function ScrollToTop() {
    window.scrollTo(0,0);
  }
  backButton.addEventListener('click', ScrollToTop, false);
}();
//this was an attempt at the gallery .... lightbox.
//
//THIS WAS AN ATTEMPT AT CREATING THE THUMBNAIL GRID IN JS BY PULLING DATA FROM GALLERYDATA.JS
//THIS WAS UNSUCCESSFUL
//THIS

var thumbnail = document.querySelector('.thumb');
//var loader = document.querySelector('.galleryText');

    function createGallery() {
      //debugger;
      let container = document.querySelector('.galleryContainer');
      // while(container.firstChild){
      //   container.removeChild(container.firstChild);
      // }
      galleryContent.images.forEach(function(createGallery, index) {
        //create image tag
        let galleryItem = document.createElement('img');
        //let galleryItem = document.querySelector('.thumb');
        //add classes
         galleryItem.classList.add('thumb');
        //locate images based off gallerydata,js filenames
        galleryItem.src = "assets/" + galleryContent.images[index];
        // add dataset attribute to img tag
        galleryItem.dataset.index = index;
        //pop lightbox when img tag clicked
        galleryItem.addEventListener('click', function() {showLightbox(index, galleryContent);}, false);
      });
    }

    function showLightbox(currentIndex, currentObject) {
      let lightboxImg = document.querySelector('#lightboxImg');
      let lightbox = document.querySelector('#lightbox');

      document.body.style.overflow = "hidden";

      lightbox.style.display = 'block';
      lightboxImg.src = "assets/" + currentObject.images[currentIndex];

    }
loader.addEventListener('load', createGallery, false);

if (thumbnail) {
  thumbnail.addEventListener('click', showLightbox, false);
};
//attempt at auto play for header
